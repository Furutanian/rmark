#===============================================================================
#
#	CLASS: Html
#
begin
	require 'pg'	# https://deveiate.org/code/pg/
rescue LoadError
end

module PG

	def self.connect_args

		# for OpenShift
		connect_args = false; if(	  ENV[	it = 'DATABASE_SERVICE_NAME']	|| ENV['HTTP_%s' % it])
			connect_args = {}
			connect_args[:host]		= ENV[	it = 'DATABASE_SERVICE_NAME']	|| ENV['HTTP_%s' % it]
			connect_args[:port]		= 5432
			connect_args[:dbname]	= ENV[	it = 'POSTGRESQL_DATABASE']		|| ENV['HTTP_%s' % it]
			connect_args[:user]		= ENV[	it = 'POSTGRESQL_USER']			|| ENV['HTTP_%s' % it]
			connect_args[:password]	= ENV[	it = 'POSTGRESQL_PASSWORD']		|| ENV['HTTP_%s' % it]

		# for HEROKU
		elsif(it = ENV['DATABASE_URL'])
			require 'uri'
			uri = URI.parse(it)
			if(uri.scheme == 'postgres')
				connect_args = {}
				connect_args[:host]		= uri.host
				connect_args[:port]		= uri.port
				connect_args[:dbname]	= uri.path[1..-1]
				connect_args[:user]		= uri.user
				connect_args[:password]	= uri.password
			end
		end
		connect_args
	end

	class Connection

		def results_as_hash=(bool)
			@results_as_hash = bool
		end

		def execute(sql)
			result = self.exec(sql)
			result.results_as_hash = @results_as_hash
			result
		end
	end

	class Result

		def size
			self.ntuples
		end

		def results_as_hash=(bool)
			@results_as_hash = bool
		end

		def [](n, m = nil)
			rs = []; (m ? m : 1).times {|i|
				r = @results_as_hash ? {} : []
				self.tuple(n + i).each {|key, value|
					@results_as_hash ? r[key] = value : r << value
				} rescue(break)
				rs << r
			}
			m ? rs : rs[0]
		end
	end
end

